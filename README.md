# Bike Sharing Demand

This is a Notebook for the Kaggle competition, bike sharing demand: 

https://www.kaggle.com/competitions/bike-sharing-demand

## Goal

The goal of notebook is to predict the total count of bikes rented during each hour covered by the test set, using only information available prior to the rental period.

## Evaluation

Submissions are evaluated one the Root Mean Squared Logarithmic Error (RMSLE). 

## Main Contents 

- Linear Regression
- XGB Regressor

## Kaggle's notebook:

https://www.kaggle.com/code/mr0024/bike-sharing-using-lr-and-xgb

## Requirements

The requirements file contains all the packages needed to work through this notebook
